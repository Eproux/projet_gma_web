<?php
session_start();

require_once("util/class.PdoGma.inc.php");
include("vues/v_entete.php") ;
include("vues/v_bandeau.php") ;
require_once("util/ClasseCartes.php") ;
require_once("util/ClasseClub.php") ;
require_once("util/ClasseJoueur.php") ;
require_once("util/ClasseTrou.php") ;


if(!isset($_REQUEST['uc']))
    $uc = 'accueil';
else

    $uc = $_REQUEST['uc'];

$pdo = PdoGma::getPdoGma() ;
switch($uc)
{
    case 'accueil':
    {include("vues/v_accueil.php");break;}

    case'connexion':
    {
        include("controleurs/c_connexion.php");break;}

    case'parcours':
    {if(isset($_SESSION['Connexion'])){
        include("controleurs/c_parcours.php");}else{$message="Veuillez vous connecter pour accéder à cette page";include("vues/v_message.php");}break;}

    case'infoparcours':
    {if(isset($_SESSION['Connexion'])){
        include("controleurs/c_infoParcours.php");}else{$message="Veuillez vous connecter pour accéder à cette page";include("vues/v_message.php");}break;}

    case'infoclubs':
    {if(isset($_SESSION['Connexion'])){
        include("controleurs/c_infoClubs.php");}else{$message="Veuillez vous connecter pour accéder à cette page";include("vues/v_message.php");}break;}

    case'listeclubs':
    {if(isset($_SESSION['Connexion'])){
        include("controleurs/c_clubs.php");}else{$message="Veuillez vous connecter pour accéder à cette page";include("vues/v_message.php");}break;}

    case'classementparcours':
    {if(isset($_SESSION['Connexion'])){
        include("controleurs/c_classementParcours.php");}else{$message="Veuillez vous connecter pour accéder à cette page";include("vues/v_message.php");}break;}

    case'classementclubs':
    {if(isset($_SESSION['Connexion'])){
        include("controleurs/c_classementClubs.php");}else{$message="Veuillez vous connecter pour accéder à cette page";include("vues/v_message.php");}break;}

    case'search':
    {if(isset($_SESSION['Connexion'])){include("./vues/v_recherche.php");}else{$message="Veuillez vous connecter pour accéder à cette page";include("vues/v_message.php");}break;}

	case'deconnexion':
	{
        include("controleurs/c_connexion.php");break;}


}
include("vues/v_pied.php") ;
?>