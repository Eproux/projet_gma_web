<?php

class Carte

{
    private $dateCarte;
    private $score ;
    private $commentaire;
    private $leParcours;
    private $leJoueur;



    public function setdateCarte($dateCarte)
    {
        $this->dateCarte = $dateCarte;
    }
    public function getdateCarte()
    {
        return $this->dateCarte;
    }

    public function setscore($score)
    {
        $this->score = $score;
    }
    public function getscore()
    {
        return $this->score;
    }

    public function setcommentaire($commentaire)
    {
        $this->commentaire = $commentaire;
    }
    public function getcommentaire()
    {
        return $this->commentaire;
    }

    public function setParcours($P)
    {
        $this->leParcours = $P;
    }
    public function getParcours()
    {
        return $this->leParcours;
    }

    public function setJoueur($J)
    {
        $this->leJoueur = $J;
    }
    public function getJoueur()
    {
        return $this->leJoueur;
    }


    public function Carte($uneDateCarte,$unScore,$unCom,$unParcours,$unJoueur){

        $this->setParcours($unParcours);
        $this->setJoueur($unJoueur);
        $this->setcommentaire($unCom);
        $this->setscore($unScore);
        $this->setdateCarte($uneDateCarte);

    }


}

?>