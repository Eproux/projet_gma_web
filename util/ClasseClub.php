<?php

class Club

{
    private $idClub;
    private $nom;
    private $rue;
    private $cp;
    private $ville;
    private $lat;
    private $longi;
    private $telephone;
    private $email;
    private $employes;
    private $code;
    private $LesTrous=array();

    public function setidClub($idClub)
    {
        $this->idClub = $idClub;
    }
    public function getidClub()
    {
        return $this->idClub;
    }

    public function getLesTrous()
    {
        return $this->LesTrous;
    }

    public function setLesTrous($Trous)
    {
         $this->LesTrous=$Trous;
    }


    public function setnom($nom)
    {
        $this->nom = $nom;
    }
    public function getnom()
    {
        return $this->nom;
    }

    public function setrue($rue)
    {
        $this->rue = $rue;
    }
    public function getrue()
    {
        return $this->rue;
    }

    public function setcp($cp)
    {
        $this->cp = $cp;
    }
    public function getcp()
    {
        return $this->cp;
    }

    public function setville($ville)
    {
        $this->ville = $ville;
    }
    public function getville()
    {
        return $this->ville;
    }

    public function setlat($lat)
{
    $this->lat = $lat;
}
    public function getlat()
    {
        return $this->lat;
    }

    public function setlongi($longi)
    {
        $this->longi = $longi;
    }
    public function getlongi()
    {
        return $this->longi;
    }

    public function settelephone($telephone)
    {
        $this->telephone = $telephone;
    }
    public function gettelephone()
    {
        return $this->telephone;
    }

    public function setemail($email)
    {
        $this->email = $email;
    }
    public function getemail()
    {
        return $this->email;
    }

    public function setemployes($employes)
    {
        $this->employes = $employes;
    }
    public function getemployes()
    {
        return $this->employes;
    }

    public function setcode($code)
    {
        $this->code = $code;
    }
    public function getcode()
    {
        return $this->code;
    }


    public function Club($unIdC,$unNom,$uneRue,$unCp,$uneVille,$uneLat,$uneLong,$unTel,$unMail,$E,$unCode,$Trou){

        $this->setidClub($unIdC);
        $this->setnom($unNom);
        $this->setrue($uneRue);
        $this->setcp($unCp);
        $this->setville($uneVille);
        $this->setlat($uneLat);
        $this->setlongi($uneLong);
        $this->settelephone($unTel);
        $this->setemail($unMail);
        $this->setemployes($E);
        $this->setcode($unCode);
        $this->setLesTrous($Trou);



    }



}


?>