<?php

class Joueur

{
    private $idJoueur;
    private $nom;
    private $prenom;
    private $dateNaissance;
    private $adresse;
    private $telephone;


    public function setidJoueur($idJoueur)
    {
        $this->idJoueur = $idJoueur;
    }
    public function getidJoueur()
    {
        return $this->idJoueur;
    }


    public function setnom($nom)
    {
        $this->nom = $nom;
    }
    public function getnom()
    {
        return $this->nom;
    }

    public function setprenom($prenom)
    {
        $this->prenom = $prenom;
    }
    public function getprenom()
    {
        return $this->prenom;
    }


    public function setdateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;
    }
    public function getdateNaissance()
    {
        return $this->dateNaissance;
    }


    public function setadresse($adresse)
    {
        $this->adresse = $adresse;
    }
    public function getadresse()
    {
        return $this->adresse;
    }

    public function settelephone($telephone)
    {
        $this->telephone = $telephone;
    }
    public function gettelephone()
    {
        return $this->telephone;
    }


    public function Joueur($unIdJ,$unNom,$unPrenom,$uneDateNaissance,$uneAdresse,$unTel){

        $this->setidJoueur($unIdJ);
        $this->setnom($unNom);
        $this->setprenom($unPrenom);
        $this->setdateNaissance($uneDateNaissance);
        $this->setadresse($uneAdresse);
        $this->settelephone($unTel);

    }




}


?>