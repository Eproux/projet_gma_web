<?php

class Trou

{
    private $idTrou;
    private $distance;
    private $par;
    private $idClub;
    private $scoreTrou;


    public function setidTrou($idTrou)
{
    $this->idTrou = $idTrou;
}
    public function getidTrou()
    {
        return $this->idTrou;
    }

    public function setscore($idscore)
    {
        $this->scoreTrou = $idscore;
    }
    public function getscore()
    {
        return $this->scoreTrou;
    }

    public function setdistance($distance)
    {
        $this->distance = $distance;
    }
    public function getdistance()
    {
        return $this->distance;
    }

    public function setpar($par)
    {
        $this->par = $par;
    }
    public function getpar()
    {
        return $this->par;
    }


    public function setidClub($idClub)
    {
        $this->idClub = $idClub;
    }
    public function getidClub()
    {
        return $this->idClub;
    }


    public function Trou($unID,$uneDistance,$unPar,$unIdC){

        $this->setidTrou($unID);
        $this->setdistance($uneDistance);
        $this->setpar($unPar);
        $this->setidClub($unIdC);

    }
}


?>