<?php

class PdoGma
{
    private static $serveur = 'mysql:host=localhost';
    private static $bdd = 'dbname=u882925972_gma';
    private static $user = 'u882925972_enzo';
    private static $mdp = 'Enzo0632066458';
    private static $monPdo;
    private static $monPdoGma = null;

    /**
     * Constructeur privé, crée l'instance de PDO qui sera sollicitée
     * pour toutes les méthodes de la classe
     */
    private function __construct()
    {
        PdoGma::$monPdo = new PDO(PdoGma::$serveur . ';' . PdoGma::$bdd, PdoGma::$user, PdoGma::$mdp);
        PdoGma::$monPdo->query("SET CHARACTER SET utf8");
    }

    public function _destruct()
    {
        PdoGma::$monPdo = null;
    }
    /**
     * Fonction statique qui crée l'unique instance de la classe
     *
     * Appel : $instancePdolafleur = PdoLafleur::getPdoLafleur();
     * @return l'unique objet de la classe PdoLafleur
     */
    public static function getPdoGma()
    {
        if (PdoGma::$monPdoGma == null) {
            PdoGma::$monPdoGma = new PdoGma();
        }
        return PdoGma::$monPdoGma;
    }

    public function testConnexion($login, $mdp)
    {
        $req = "select * from joueur where login='" . $login . "' and mdp ='" . $mdp . "' ;";
        $res = PdoGma::$monPdo->query($req);
        $Resu = $res->fetchAll();
        return $Resu;
    }


    public function GetParcours($id)
    {

        $req="select * from cartes where IdJoueur=$id ; ";
        $res = PdoGma::$monPdo->query($req);
        $lesParcours=$res->fetchAll();
        return $lesParcours;

    }


    public function GetClubParcours($idCarte){

        $req="select * from club where idClub=(select idClub from cartes where idCartes =$idCarte);";
        $res = PdoGma::$monPdo->query($req);
        $lesParcours=$res->fetchAll();
        return $lesParcours;


    }


    public function GetClub($recherche)
    {
        $req = "SELECT * FROM club WHERE ville LIKE '%$recherche%' or nom LIKE '%$recherche%' or cp LIKE '%$recherche%'";
        $res = PdoGma::$monPdo->query($req);
        $lesClubs = $res->fetchAll();
        return $lesClubs;
    }


    public function GetTrousClub($idClub){


        $req = "select * from trou,club where trou.idCLub = club.idClub and club.idClub=$idClub ;";
        $res = PdoGma::$monPdo->query($req);
        $lesTrous = $res->fetchAll();
        return $lesTrous;

    }

    public function GetTrousClubCarte($id,$idCarte){

        $req = "select posseder.idTrou,distance,par,trou.idClub,scoreTrou from trou,club,posseder where club.idClub=trou.idClub and trou.idTrou = posseder.idTrou  and club.idClub=$id and idCartes=$idCarte;";
        $res = PdoGma::$monPdo->query($req);
        $lesClubs = $res->fetchAll();
        return $lesClubs;



    }

    public function GetInfoClubs($idClub)
    {
        $req="select * from club where idClub='".$idClub."' ;";
        $res = PdoGma::$monPdo->query($req);
        $lesInfoClubs=$res->fetchAll();
        return $lesInfoClubs;

    }

    public function GetCarteParcours($idCarte)
    {
        $req="select * from cartes where idCartes='".$idCarte."'";
        $res = PdoGma::$monPdo->query($req);
        $lesInfoParcours=$res->fetchAll();
        return $lesInfoParcours;

    }

    public function GetInfoClassementClub($idClub)
    {
        $req="select * from cartes,joueur where cartes.IdJoueur = joueur.IdJoueur and cartes.idClub = $idClub ; ";
        $res = PdoGma::$monPdo->query($req);
        $lesInfoParcours=$res->fetchAll();
        return $lesInfoParcours;

    }




    public function GetInfoClassementJoueur($idClub,$idJ)
    {
        $req="select score from cartes where IdJoueur =$idJ and idClub='".$idClub."' ;";
        $res = PdoGma::$monPdo->query($req);
        $lesInfoClassementParcours=$res->fetch();
        return $lesInfoClassementParcours;

    }

    //select idCartes from cartes group by score asc


    public function recupInfoClassementParcours($idTrou)
    {
        $req="select score,nom,prenom,idTrou from cartes,joueur,trou where idTrou='".$idTrou."'";
        $res=PdoGma::$monPdo->query($req);
        $lesRecupInfoClassementParcours=$res->fetchAll();
        return $lesRecupInfoClassementParcours;
   }



    public function GetInfoClassementClubs($idClub)
    {
        $req="select score from cartes where idClub='".$idClub."' ;";
        $res = PdoGma::$monPdo->query($req);
        $lesInfoClassementClubs=$res->fetch();
        return $lesInfoClassementClubs;

    }


    public function recupNbJoueurClub($idClub)
    {
        $req="select count(*) from cartes where idClub='".$idClub."'";
        $res=PdoGma::$monPdo->query($req);
        $lesrecupNbJoueurClub=$res->fetch();
        return $lesrecupNbJoueurClub;
    }


}


?>